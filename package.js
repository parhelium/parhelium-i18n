Package.describe( {
    name: 'parhelium:i18n',
    version: '0.0.1',
    summary: '',
    git: '',
    documentation: 'README.md'
} );

client = ['client'];
server = ['server'];
both = ['client', 'server'];

Package.onUse( function ( api ) {
    api.versionsFrom( '1.1.0.2' );

    api.use( [
        'underscore',
        'mongo',
        'parhelium:logger',
        'parhelium:js-schema',
        'parhelium:helpers'
    ], both );

    api.addFiles( 'i18n.js', both );
    api.export('i18n')
} );

Package.onTest( function ( api ) {
    api.use( [
        'underscore',
        'mongo',
        'parhelium:i18n',
        'parhelium:logger',
        'parhelium:js-schema',
        'tinytest',
        'parhelium:helpers'
    ], both );

    api.addFiles( 'i18n.js', both );
    api.export('i18n')
    api.addFiles( 'test/i18n-tests.js', server );
} );
