Tinytest.add( "i18n : invalid argument check", function ( test ) {
    [{}, 2, "cwa", []].forEach( function ( arg ) {
        test.throws( function () { i18n( arg ) } )
    } )
} )

var Collection = i18n( new Mongo.Collection( 'i18n : adds i18nFind method to Mongo.Collection' ) );

Tinytest.add( "i18n : adds i18nFind method to Mongo.Collection", function ( test ) {
    test.isTrue( Collection != null, 'i18n should return Mongo.Collection, not NULL' )
    test.isTrue(
        _.isFunction( Collection.i18nFind ),
        "i18n wrapper should extend Mongo.Collection with i18nFind function."
    )
} );

var basicFlatDoc = {
    _id: "wa",
    a: "a",
    b: "b",
    i18n: {
        'en_EN': {
            a: 'en_a',
        },
        'de_DE': {
            a: 'de_a',
        }
    }
};

// it doesn't matter if lang is declared as pl or pl_PL
var langConfig = {
    supported: ["pl_PL", "en_EN", "de_DE"],
    baseLanguage: "pl_PL"
}

var clone = function ( obj ) {
    return _.extend( {}, obj );
}

Tinytest.add( "i18n._generateTransform : basicFlatDoc ", function ( test ) {
    var basicFlatDocWithouti18n = clone( basicFlatDoc );
    delete basicFlatDocWithouti18n.i18n;

    test.equal(
        i18n._generateTransform( {}, _.extend( {}, langConfig, { language: 'en_EN' } ) )( clone( basicFlatDoc ) ),
        _.extend( {}, basicFlatDocWithouti18n, {
            a: 'en_a'
        } )
    )

    test.equal(
        i18n._generateTransform( {}, _.extend( {}, langConfig, { language: 'de_DE' } ) )( clone( basicFlatDoc ) ),
        _.extend( {}, basicFlatDocWithouti18n, {
            a: 'de_a'
        } )
    )
} );

Tinytest.add( "i18n._generateFields : basicFlatDoc : all fields", function ( test ) {
    var fields = {}
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'en_EN' } ) ),
        { 'i18n.de_DE': 0 }
    )
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'de_DE' } ) ),
        { 'i18n.en_EN': 0 }
    )
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, {} ) ),
        { 'i18n': 0 }
    )
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'pl_PL' } ) ),
        { 'i18n': 0 }
    )
} );

Tinytest.add( "i18n._generateFields : basicFlatDoc : whitelisted fields", function ( test ) {
    var fields = { b: 1 }
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'en_EN' } ) ),
        { b: 1, 'i18n.en_EN.b': 1 }
    )
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'pl_PL' } ) ),
        { 'b': 1 }
    )
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, {} ) ),
        { 'b': 1 }
    )
} );

Tinytest.add( "i18n._generateFields : basicFlatDoc : blacklisted fields", function ( test ) {
    var fields = { b: 0 }
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'en_EN' } ) ),
        { b: 0, 'i18n.en_EN.b': 0, 'i18n.de_DE': 0 }
    )
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'pl_PL' } ) ),
        { 'b': 0, 'i18n': 0 }
    )
} );


var nestedDoc = {
    _id: "wa",
    a: {
        aa: {
            aaa: 'pl_PL_aaa'
        },
        bb: "b",
    },
    i18n: {
        'en_EN': {
            a: {
                aa: {
                    aaa: 'en_EN_aaa'
                },
            },
        },
        'de_DE': {
            a: {
                aa: {
                    aaa: 'de_DE_aaa'
                },
            },
        }
    }
};

Tinytest.add( "i18n._generateTransform : nestedDoc ", function ( test ) {
    var nestedDocWithouti18n = _.extend( {}, nestedDoc );
    delete nestedDocWithouti18n.i18n;
    var transform = i18n._generateTransform( {}, _.extend( {}, langConfig, { language: 'en_EN' } ) );
    var result = _.extend( {}, nestedDocWithouti18n, {
        a: {
            aa: {
                aaa: 'en_EN_aaa'
            },
            bb: 'b'
        }
    } )
    test.equal( transform( clone( nestedDoc ) ), result )
} );

Tinytest.add( "i18n._generateFields : nestedDoc : whitelisted fields", function ( test ) {
    var fields = { 'a.aa.aaa': 1 }
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'en_EN' } ) ),
        { 'a.aa.aaa': 1, 'i18n.en_EN.a.aa.aaa': 1 }
    )

} );

Tinytest.add( "i18n._generateFields : nestedDoc : blacklisted fields", function ( test ) {
    var fields = { 'a.aa.aaa': 0 }
    test.equal(
        i18n._generateFields( { fields: fields }, _.extend( {}, langConfig, { language: 'en_EN' } ) ),
        { 'a.aa.aaa': 0, 'i18n.en_EN.a.aa.aaa': 0, 'i18n.de_DE': 0 }
    )
} );

/**
 * language:String is optional
 * baseLanguage:String is mandatory and must be included in supported
 * supported:Array and is mandatory and must have at least 1 language
 */
Tinytest.add( "i18n._validateL8E : invalid  argument check", function ( test ) {
    test.throws( function () { i18n._validateL8E( null ) } )
    test.throws( function () { i18n._validateL8E( {} ) } )
    test.throws( function () { i18n._validateL8E( {language:"dwa", baseLanguage:'daw'} ) } )
    test.throws( function () { i18n._validateL8E( {language:"dwa", baseLanguage:null, supported:'dwa' } ) } )
    test.throws( function () { i18n._validateL8E( {language:"dwa", baseLanguage:null, supported:[] } ) } )
    test.throws( function () { i18n._validateL8E( {language:"a", baseLanguage:'b', supported:['a'] } ) } )

} );
//
//Tinytest.add( "i18n.configure : retrieves configuration", function ( test ) {
//    test.isTrue( false )
//} );
//
//Tinytest.add( "i18n.languages", function ( test ) {
//    test.isTrue( false )
//} );
//
//Tinytest.add( "i18n.languagesArray", function ( test ) {
//    test.isTrue( false )
//} );
//
//Tinytest.add( "Collection.i18nFind : all fields : flat structure", function ( test ) {
//    test.isTrue( false )
//} );
//
//Tinytest.add( "Collection.i18nFind : all fields : nested fields structure", function ( test ) {
//    test.isTrue( false )
//} );
//
//Tinytest.add( "Collection.i18nFind : whitelisted fields", function ( test ) {
//    test.isTrue( false )
//} );
//
//Tinytest.add( "Collection.i18nFind : blacklisted fields", function ( test ) {
//    test.isTrue( false )
//} );