var logger = loggerFactory( 'i18n_server' );

var languageSchema = schema( {
    '?language': String,
    supported: Array.of( String ),
    baseLanguage: String
} );

/*  Copyright (C) 2012-2014  Kurt Milam - http://xioup.com | Source: https://gist.github.com/1868955
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

var deepExtend = function ( obj ) {
    var parentRE = /#{\s*?_\s*?}/,
        slice = Array.prototype.slice;

    _.each( slice.call( arguments, 1 ), function ( source ) {
        for ( var prop in source ) {
            if ( _.isUndefined( obj[prop] ) || _.isFunction( obj[prop] ) || _.isNull( source[prop] ) || _.isDate( source[prop] ) ) {
                obj[prop] = source[prop];
            }
            else if ( _.isString( source[prop] ) && parentRE.test( source[prop] ) ) {
                if ( _.isString( obj[prop] ) ) {
                    obj[prop] = source[prop].replace( parentRE, obj[prop] );
                }
            }
            else if ( _.isArray( obj[prop] ) || _.isArray( source[prop] ) ) {
                if ( !_.isArray( obj[prop] ) || !_.isArray( source[prop] ) ) {
                    throw new Error( 'Trying to combine an array with a non-array (' + prop + ')' );
                } else {
                    obj[prop] = _.reject( _.deepExtend( _.clone( obj[prop] ), source[prop] ), function ( item ) { return _.isNull( item );} );
                }
            }
            else if ( _.isObject( obj[prop] ) || _.isObject( source[prop] ) ) {
                if ( !_.isObject( obj[prop] ) || !_.isObject( source[prop] ) ) {
                    throw new Error( 'Trying to combine an object with a non-object (' + prop + ')' );
                } else {
                    obj[prop] = _.deepExtend( _.clone( obj[prop] ), source[prop] );
                }
            } else {
                obj[prop] = source[prop];
            }
        }
    } );
    return obj;
};

_.mixin( { 'deepExtend': deepExtend } );


i18n = function ( collection ) {
    if ( !(collection instanceof Mongo.Collection) ) {
        throw new Error( 'Passed argument collection is not instance of Mongo.Collection' )
    }
    collection.i18nFind = i18n.find.bind( collection );
    collection.i18nFindOne = i18n.findOne.bind( collection );
    return collection;
}

i18n.find = function ( l8e, selector, options ) {
    var opts;
    if ( Meteor.isClient ) {
        opts = _.extend( {}, options, { transform: i18n._generateTransform( options, l8e ) } );
        return this.find( selector, opts )
    }
    if ( Meteor.isServer ) {
        opts = _.extend( {}, options, { fields: i18n._generateFields( options, l8e ) } );
        return this.find( selector, opts )
    }
}

i18n.findOne = function ( l8e, selector, options ) {
    var opts;
    if ( Meteor.isClient ) {
        opts = _.extend( {}, options, { transform: i18n._generateTransform( options, l8e ) } );
        return this.findOne( selector, opts )
    }
    if ( Meteor.isServer ) {
        opts = _.extend( {}, options, { fields: i18n._generateFields( options, l8e ) } );
        return this.findOne( selector, opts )
    }
}

i18n._generateTransform = function ( options, l8e ) {
    i18n._validateL8E( l8e );

    return function ( doc ) {
        var obj = _.extend( {}, H.pathToValue( doc, 'i18n.' + l8e.language ) );
        delete doc.i18n;
        return _.deepExtend( doc, obj );
    }
}
i18n._validateL8E = function ( l8e ) {
    if ( l8e == null )
        throw new Error( 'l8e cannot be null' )

    if ( _.isString( l8e ) )
        throw new Error( 'l8e must be object, not string' )

    if ( !languageSchema( l8e ) )
        throw new Error( 'Passed l8e is not valid : ', languageSchema.errors( l8e ) );

    if ( l8e.supported.length < 1 )
        throw new Error( 'l8e.supported.length cannot be less than 1' );

    if ( !l8e.baseLanguage )
        throw new Error( 'l8e.baseLanguage must be defined' );

    if ( l8e.supported.indexOf( l8e.baseLanguage ) == -1 )
        throw new Error( 'l8e.baseLanguage must be in supported language (contained in l8e.supported) ' );

    if ( l8e.language && l8e.supported.indexOf( l8e.language ) == -1 )
        throw new Error( 'l8e.language must be in supported language (contained in l8e.supported) \n' + H.pretty( l8e ) );
};

i18n._generateFields = function ( options, l8e ) {
    if ( Meteor.isClient )
        throw new Error( 'Can run only on server side' )

    var originalFields = options && options.fields || {};

    var i18nFields = _.extend( {}, originalFields );
    var lang, field;


    i18n._validateL8E( l8e );


    if ( !_.isEmpty( i18nFields ) ) {
        delete i18nFields._id;
        var white_list_projection = _.first( _.values( i18nFields ) ) === 1;
        if ( "_id" in originalFields ) {
            i18nFields["_id"] = originalFields["_id"];
        }
        if ( white_list_projection ) {
            for ( field in originalFields ) {
                if ( field !== "_id" &&
                    l8e.language != l8e.baseLanguage &&
                    l8e.language != null
                ) {
                    i18nFields["i18n." + l8e.language + "." + field] = 1;
                }
            }
        } else {
            if ( l8e.language == null || l8e.language == l8e.baseLanguage ) {
                i18nFields.i18n = 0;
            } else {
                for ( var i = 0; i < l8e.supported.length; i++ ) {
                    lang = l8e.supported[i];
                    if ( lang != l8e.baseLanguage ) {
                        if ( lang != l8e.language ) {
                            i18nFields["i18n." + lang] = 0;
                        } else {
                            for ( field in originalFields ) {
                                if ( field !== "_id" ) {
                                    i18nFields["i18n." + l8e.language + "." + field] = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        if ( l8e.language == null || l8e.language == l8e.baseLanguage ) {
            i18nFields.i18n = 0;
        } else {
            for ( var i = 0; i < l8e.supported.length; i++ ) {
                lang = l8e.supported[i];
                if (
                    lang != l8e.baseLanguage &&
                    lang != l8e.language
                ) {
                    i18nFields["i18n." + lang] = 0;
                }
            }
        }
    }
    return i18nFields;
}
